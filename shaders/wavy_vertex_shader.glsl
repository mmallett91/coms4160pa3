uniform vec3 time;

varying float intensityX, intensityY, intensityZ;
void main () {

	intensityX = gl_Vertex.x;
	intensityY = gl_Vertex.y;
	intensityZ = gl_Vertex.z;
	vec4 v = gl_Vertex;
	
	float s = 0.05 * sin(3.0 * (time.x * .1 + v.y));
	
	v.x = v.x + s;
	v.w = 1.0;
	gl_Position = gl_ModelViewProjectionMatrix * v;
}