uniform vec3 lights;
uniform vec3 light1;
uniform vec3 light2;
uniform vec3 light3;

uniform vec3 diffuseColor;
uniform vec3 specColor;
uniform vec3 shinyVec;

varying vec3 vertPos;
varying vec3 normalInterp;

void main (){

	vec3 ambientColor = vec3(0.0, 0.0, 0.3);
	float shiny = shinyVec.x;
	
	vec3 normal = normalize(normalInterp);
	
	
	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);
	
	int lights = int(lights.x);
	
	for(int i=0; i<lights; i++){
	
		vec3 lightPos;
		if(i==0){
			lightPos = vec3(light1);
		}
		if(i==1){
			lightPos = vec3(light2);
		}
		if(i==2){
			lightPos = vec3(light3);
		}
		
		vec3 lightDir = normalize(lightPos - vertPos);
		vec3 reflectDir = reflect(-lightDir, normal);
		vec3 viewDir = normalize(-vertPos);
		
		float lambertian = max(dot(lightDir, normal), 0.0);
		
		if(lambertian > 0.0){
		
			float specAngle = max(dot(reflectDir, viewDir), 0.0);
			specular += specColor * pow(specAngle, shiny);
		
		}
		
		diffuse += lambertian * diffuseColor;
	
	}
	
	gl_FragColor = vec4(ambientColor + diffuse + specular, 1.0);

}