uniform vec3 time;

varying vec3 v;

void main () {
	v.x = gl_Vertex.x;
	v.y = gl_Vertex.y;
	v.z = gl_Vertex.z;
	gl_Position = ftransform();
}