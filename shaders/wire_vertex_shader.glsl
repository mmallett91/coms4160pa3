varying float intensityX, intensityY, intensityZ;

varying vec2 texture_coordinate;

void main () {
	intensityX = gl_Vertex.x;
	intensityY = gl_Vertex.y;
	intensityZ = gl_Vertex.z;
	gl_Position = ftransform();
	
	texture_coordinate = vec2(gl_MultiTexCoord0);
}