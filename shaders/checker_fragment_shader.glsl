varying float intensityX, intensityY, intensityZ;

varying vec2 texture_coordinate;

void main () {

	float delta = .02;
	
	vec4 c1 = vec4(0.2, 0.7, 0.7, 1.0);
	vec4 c2 = vec4(0.7, 0.2, 0.7, 1.0);

	if(texture_coordinate.x > 0.5){
		if(texture_coordinate.y > 0.5){
			gl_FragColor = c1;
		}
		else{
			gl_FragColor = c2;
		}
	}
	else{
		if(texture_coordinate.y > 0.5){
			gl_FragColor = c2;
		}
		else{
			gl_FragColor = c1;
		}
	}
	
}