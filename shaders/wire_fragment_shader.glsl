varying float intensityX, intensityY, intensityZ;

varying vec2 texture_coordinate;

void main () {

	float delta = .02;

	if(texture_coordinate.x < delta || (1.0 - texture_coordinate.x < delta) || 
		texture_coordinate.y < delta || (1.0 - texture_coordinate.y < delta) ||
		abs(texture_coordinate.x - texture_coordinate.y) < delta){
		gl_FragColor = vec4 (texture_coordinate, 0.0, 1.0);
	}
	else{
		discard;
	}

	
}