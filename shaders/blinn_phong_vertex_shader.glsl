uniform vec3 lights;
uniform vec3 light1;
uniform vec3 light2;
uniform vec3 light3;

varying vec3 vertPos;
varying vec3 normalInterp;

void main(void)  
{     
   vertPos = vec3(gl_ModelViewMatrix * gl_Vertex);       
   normalInterp = normalize(gl_NormalMatrix * gl_Normal);
   gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;  
}