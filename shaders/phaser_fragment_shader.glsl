uniform vec3 time;

varying vec3 v;

void main () {
	float y = 1.5 * sin(time.x * .01);
	
	if(v.y > y){
		discard;
	}
	else{
		gl_FragColor = vec4 (v.x, v.y, v.z, 1.0);
	}
	
}