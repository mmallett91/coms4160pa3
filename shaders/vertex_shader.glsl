varying float intensityX, intensityY, intensityZ;
void main () {
	intensityX = gl_Vertex.x;
	intensityY = gl_Vertex.y;
	intensityZ = gl_Vertex.z;
	gl_Position = ftransform();
}