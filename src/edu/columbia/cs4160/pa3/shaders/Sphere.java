package edu.columbia.cs4160.pa3.shaders;

import org.lwjgl.opengl.GL11;

public class Sphere {
	
	private float radius;
	private int stacks;
	private int slices;
	
	public Sphere(float radius, int stacks, int slices){
		this.radius = radius;
		this.slices = slices;
		this.stacks = stacks;
	}
	
	public void draw(){
		
		/*
		 	x= r sin(theta)cos(phi)
			y= r sin(theta)sin(phi)
			z= r cos(theta)
		 */
		
		float[] texCoord = {
				0.0f, 0.0f,
				0.0f, 1.0f,
				1.0f, 0.0f,
				1.0f, 1.0f
		};
		
		for(int theta=0; theta<=360 ; theta += 20){
			for(int phi=0; phi<=180; phi += 20){
			
				int theta2 = theta + 20;
				int phi2 = phi + 20;
				
				float x1 = (float) (radius * Math.sin(rad(theta)) * Math.sin(rad(phi)));
				float x2 = (float) (radius * Math.sin(rad(theta)) * Math.sin(rad(phi2)));
				float x3 = (float) (radius * Math.sin(rad(theta2)) * Math.sin(rad(phi)));
				float x4 = (float) (radius * Math.sin(rad(theta2)) * Math.sin(rad(phi2)));
				
				float z1 = (float) (radius * Math.cos(rad(theta)) * Math.sin(rad(phi)));
				float z2 = (float) (radius * Math.cos(rad(theta)) * Math.sin(rad(phi2)));
				float z3 = (float) (radius * Math.cos(rad(theta2)) * Math.sin(rad(phi)));
				float z4 = (float) (radius * Math.cos(rad(theta2)) * Math.sin(rad(phi2)));
				
				float y1 = (float) (radius * Math.cos(rad(phi)));
				float y2 = (float) (radius * Math.cos(rad(phi2)));
				
				GL11.glBegin(GL11.GL_TRIANGLES);
				
				
//				float[] norm1 = avgNormal(
//						new float[] {x1, x2, x3},
//						new float[] {y1, y2, y1},
//						new float[] {z1, z2, z3});
//				
//				float[] norm2 = avgNormal(
//						new float[] {x3, x4, x2},
//						new float[] {y1, y2, y2},
//						new float[] {z3, z4, z2});
				
				
//				GL11.glNormal3f(norm1[0], norm1[1], norm1[2]);
				GL11.glTexCoord2f(texCoord[2], texCoord[3]);
				GL11.glVertex3f(x2, y2, z2);
				
				GL11.glTexCoord2f(texCoord[4], texCoord[5]);
				GL11.glVertex3f(x3, y1, z3);
				
				GL11.glTexCoord2f(texCoord[0], texCoord[1]);
				GL11.glVertex3f(x1, y1, z1);
				
				
//				GL11.glNormal3f(norm2[0], norm2[1], norm2[2]);
				GL11.glTexCoord2f(texCoord[4], texCoord[5]);
				GL11.glVertex3f(x3, y1, z3);
				
				GL11.glTexCoord2f(texCoord[6], texCoord[7]);
				GL11.glVertex3f(x4, y2, z4);
				
				GL11.glTexCoord2f(texCoord[2], texCoord[3]);
				GL11.glVertex3f(x2, y2, z2);
			
				GL11.glEnd();
				
			}
		
		}
		
	}
	
	private double rad(int deg){
		return (double) (deg * Math.PI) / 180.0;
	}
	
	private float[] avgNormal(float[] p1, float[] p2, float[] p3){
		
		return normalize(new float[] {
			(p1[0] + p2[0] + p3[0]) / 3.0f,
			(p1[1] + p2[1] + p3[1]) / 3.0f,
			(p1[2] + p2[2] + p3[2]) / 3.0f
		});
		
	}
	
	private float[] normalize(float[] v){
		
		float mag = (float) Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
		
		return new float[] {
				v[0] / mag,
				v[1] / mag,
				v[2] / mag
		};
		
	}

}
