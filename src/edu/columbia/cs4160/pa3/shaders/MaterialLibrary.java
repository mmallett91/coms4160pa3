package edu.columbia.cs4160.pa3.shaders;

import java.util.ArrayList;

public class MaterialLibrary {
	
	private int index;
	
	private ArrayList<Material> mats;
	
	public MaterialLibrary(){
		index = 0;
		mats = new ArrayList<Material>();
		
		Material plastic = new Material(
				new float[] {0.0f, 0.0f, 0.5f},
				new float[] {1.0f, 1.0f, 1.0f},
				12f
		);
		mats.add(plastic);
		
		Material matte = new Material(
				new float[] {0.0f, 0.0f, 0.7f},
				new float[] {0.1f, 0.1f, 0.1f},
				2f
		);
		mats.add(matte);
		
		Material metal = new Material(
				new float[] {0.0f, 0.0f, 0.2f},
				new float[] {0.3f, 0.3f, 0.9f},
				12f	
		);
		mats.add(metal);
		
		
	}
	
	public Material get(){
		return mats.get(index);
	}
	
	public void next(){
		index++;
		if(index >= mats.size()){
			index = 0;
		}
	}
	
	

}
