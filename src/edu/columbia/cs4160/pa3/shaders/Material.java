package edu.columbia.cs4160.pa3.shaders;

public class Material {
	
	public final float[] diffuse;
	public final float[] specular;
	public final float shiny;
	
	public Material(float[] diffuse, float[] specular, float shiny){
		this.diffuse = diffuse;
		this.specular = specular;
		this.shiny = shiny;
	}

}
