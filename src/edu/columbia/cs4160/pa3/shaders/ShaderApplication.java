package edu.columbia.cs4160.pa3.shaders;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Vector3f;


//https://www.opengl.org/sdk/docs/tutorials/ClockworkCoders/lighting.php

public class ShaderApplication {

    String windowTitle = "3D Shapes";
    public boolean closeRequested = false;

    long lastFrameTime; // used to calculate delta
    
    float triangleAngle; // Angle of rotation for the triangles
    float quadAngle; // Angle of rotation for the quads
    float time;
    
    boolean useGoodSphere;
    
    //how many lights are on?????????
    float lights;
    
    //different materials, matte, plastic, METAL
    MaterialLibrary mats;
    

    ShaderProgram shader;
    
    public void run() {

        createWindow();
        getDelta(); // Initialise delta timer
        initGL();
        initShaders();
        
        lights = 1;
        time = 0;
        
        mats = new MaterialLibrary();
        
        while (!closeRequested) {
            pollInput();
            updateLogic(getDelta());
            renderGL();

            Display.update();
        }
        
        cleanup();
    }
    
    private void initGL() {

        /* OpenGL */
        int width = Display.getDisplayMode().getWidth();
        int height = Display.getDisplayMode().getHeight();

        GL11.glViewport(0, 0, width, height); // Reset The Current Viewport
        GL11.glMatrixMode(GL11.GL_PROJECTION); // Select The Projection Matrix
        GL11.glLoadIdentity(); // Reset The Projection Matrix
        GLU.gluPerspective(45.0f, ((float) width / (float) height), 0.1f, 100.0f); // Calculate The Aspect Ratio Of The Window
        GL11.glMatrixMode(GL11.GL_MODELVIEW); // Select The Modelview Matrix
        GL11.glLoadIdentity(); // Reset The Modelview Matrix

        GL11.glShadeModel(GL11.GL_SMOOTH); // Enables Smooth Shading
        GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Black Background
        GL11.glClearDepth(1.0f); // Depth Buffer Setup
        GL11.glEnable(GL11.GL_DEPTH_TEST); // Enables Depth Testing
        GL11.glDepthFunc(GL11.GL_LEQUAL); // The Type Of Depth Test To Do
        GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST); // Really Nice Perspective Calculations
        
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        
        initLight();
        
        Camera.create();        
    }
    
    private void initLight(){
    	
//    	float lightAmbient[] = { 1.0f, 1.0f, 1.0f, 1.0f };
//        float lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
//        float lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
//        float light2Specular[] = { 0.5f, 0.9f, 0.5f, 1.0f };
//        float light1Position[] = { 0.5f, 4.0f, 4.0f, 0.0f };
//        float light2Position[] = { 0.0f, -1.0f, 4.0f, 0.0f };
//     
//        GL11.glLight(GL11.GL_LIGHT1, GL11.GL_AMBIENT, arrayToBuffer(lightAmbient));              // Setup The Ambient Light
//        GL11.glLight(GL11.GL_LIGHT1, GL11.GL_DIFFUSE, arrayToBuffer(lightDiffuse));              // Setup The Diffuse Light
//        GL11.glLight(GL11.GL_LIGHT1, GL11.GL_SPECULAR, arrayToBuffer(lightSpecular));
//        GL11.glLight(GL11.GL_LIGHT1, GL11.GL_POSITION, arrayToBuffer(light1Position));
//        
//        GL11.glLight(GL11.GL_LIGHT2, GL11.GL_AMBIENT, arrayToBuffer(lightAmbient));              // Setup The Ambient Light
//        GL11.glLight(GL11.GL_LIGHT2, GL11.GL_DIFFUSE, arrayToBuffer(lightDiffuse));              // Setup The Diffuse Light
//        GL11.glLight(GL11.GL_LIGHT2, GL11.GL_SPECULAR, arrayToBuffer(light2Specular));
//        GL11.glLight(GL11.GL_LIGHT2, GL11.GL_POSITION, arrayToBuffer(light2Position));
//     
//	    GL11.glEnable(GL11.GL_LIGHT1);
//	    GL11.glEnable(GL11.GL_LIGHT2);
//	    GL11.glEnable ( GL11.GL_LIGHTING );
//	    
//	    numLights = 2;
    	
    }
    private FloatBuffer arrayToBuffer(float[] arr){
    	
    	FloatBuffer ret = BufferUtils.createFloatBuffer(arr.length);
    	
    	ret.put(arr);
    	ret.flip();
    	
    	return ret;
    	
    }
    
    private void initShaders() {
    	initShaders("");
    }
    
    private void initShaders(String shaderName){
    	
    	String vertexShaderPath, fragmentShaderPath;
    	if(shaderName.equals("")){
    		vertexShaderPath = "shaders/vertex_shader.glsl";
    		fragmentShaderPath = "shaders/fragment_shader.glsl";
    	}
    	else{
    		vertexShaderPath = "shaders/" + shaderName + "_vertex_shader.glsl";
    		fragmentShaderPath = "shaders/" + shaderName + "_fragment_shader.glsl";
    	}
    	
    	ShaderLoader shaderLoader = new ShaderLoader();
        String vertex_shader = shaderLoader.load(vertexShaderPath);
        
        String fragment_shader = shaderLoader.load(fragmentShaderPath);

        try {
            shader = new ShaderProgram(vertex_shader, fragment_shader);
        } catch (LWJGLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    	
    }
    
    private void updateLogic(int delta) {
    	
    	time += 1f;
    }
    
    private void initLightsOnShader(ShaderProgram shader){
    	
    	//look i support multiple lights..
    	shader.setUniform3f("lights", lights,lights,lights);
    	shader.setUniform3f("light1", 20.0f, -2.0f, 2.0f);
    	shader.setUniform3f("light2", -20.0f, 2.0f, 2.0f);
    	shader.setUniform3f("light3", 2.0f, -2.0f, 2.0f);
    	
    	float[] dif = mats.get().diffuse;
    	float[] spe = mats.get().specular;
    	float shi = mats.get().shiny;
    	
    	shader.setUniform3f("diffuseColor", dif[0], dif[1], dif[2]);
    	shader.setUniform3f("specColor", spe[0], spe[1], spe[2]);
    	shader.setUniform3f("shinyVec", shi, shi, shi);
    	
    }

    private void renderGL() {

        // start to use shaders
        shader.begin();
//        float dir = (float)(1./Math.sqrt(3));
//        shader.setUniform3f("lightDir", dir, dir, dir);
        initLightsOnShader(shader);
        shader.setUniform3f("time", time, time, time);
        
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
        GL11.glLoadIdentity(); // Reset The View
        GL11.glTranslatef(0.0f, 0.0f, -7.0f); // Move Right And Into The Screen

        Camera.apply();

        GL11.glTranslatef(-2.5f, 0, 0);
        
//        Sphere sphere = new Sphere();
        if(!useGoodSphere){
        	// i made this sphere class
        	// it sucks at lighting..
        	// BUT, it looks good under wireframe and 
        	// checkerboard shaders
	        Sphere sphere = new Sphere(1.0f, 20, 20);
	        sphere.draw();
        }
        else{
        	// use this sphere, the "good" sphere, if we need lights
        	org.lwjgl.util.glu.Sphere sphere = new org.lwjgl.util.glu.Sphere();
        	sphere.draw(1.0f, 20, 20);
        }
//        sphere.setTextureFlag(true);
//        sphere.draw(1, 20, 20);
        GL11.glTranslatef(2.25f, 0, 0);
        GL11.glBegin(GL11.GL_QUADS); // Start Drawing The Cube
        GL11.glNormal3f(0, 1, 0);
        GL11.glTexCoord2f(1, 1);
        GL11.glVertex3f(1.0f, 1.0f, -1.0f); // Top Right Of The Quad (Top)
        GL11.glTexCoord2f(0, 1);
        GL11.glVertex3f(-1.0f, 1.0f, -1.0f); // Top Left Of The Quad (Top)
        GL11.glTexCoord2f(0, 0);
        GL11.glVertex3f(-1.0f, 1.0f, 1.0f); // Bottom Left Of The Quad (Top)
        GL11.glTexCoord2f(1, 0);
        GL11.glVertex3f(1.0f, 1.0f, 1.0f); // Bottom Right Of The Quad (Top)

        GL11.glNormal3f(0, -1, 0);
        GL11.glTexCoord2f(1, 1);
        GL11.glVertex3f(1.0f, -1.0f, 1.0f); // Top Right Of The Quad (Bottom)
        GL11.glTexCoord2f(0, 1);
        GL11.glVertex3f(-1.0f, -1.0f, 1.0f); // Top Left Of The Quad (Bottom)
        GL11.glTexCoord2f(0, 0);
        GL11.glVertex3f(-1.0f, -1.0f, -1.0f); // Bottom Left Of The Quad (Bottom)
        GL11.glTexCoord2f(1, 0);
        GL11.glVertex3f(1.0f, -1.0f, -1.0f); // Bottom Right Of The Quad (Bottom)

        GL11.glNormal3f(0, 0, 1);
        GL11.glTexCoord2f(1, 1);
        GL11.glVertex3f(1.0f, 1.0f, 1.0f); // Top Right Of The Quad (Front)
        GL11.glTexCoord2f(0, 1);
        GL11.glVertex3f(-1.0f, 1.0f, 1.0f); // Top Left Of The Quad (Front)
        GL11.glTexCoord2f(0, 0);
        GL11.glVertex3f(-1.0f, -1.0f, 1.0f); // Bottom Left Of The Quad (Front)
        GL11.glTexCoord2f(1, 0);
        GL11.glVertex3f(1.0f, -1.0f, 1.0f); // Bottom Right Of The Quad (Front)

        GL11.glNormal3f(0, 0, -1);
        GL11.glTexCoord2f(0, 0);
        GL11.glVertex3f(1.0f, -1.0f, -1.0f); // Bottom Left Of The Quad (Back)
        GL11.glTexCoord2f(1, 0);
        GL11.glVertex3f(-1.0f, -1.0f, -1.0f); // Bottom Right Of The Quad (Back)
        GL11.glTexCoord2f(1, 1);
        GL11.glVertex3f(-1.0f, 1.0f, -1.0f); // Top Right Of The Quad (Back)
        GL11.glTexCoord2f(0, 1);
        GL11.glVertex3f(1.0f, 1.0f, -1.0f); // Top Left Of The Quad (Back)

        GL11.glNormal3f(-1, 0, 0);
        GL11.glTexCoord2f(1, 1);
        GL11.glVertex3f(-1.0f, 1.0f, 1.0f); // Top Right Of The Quad (Left)
        GL11.glTexCoord2f(0, 1);
        GL11.glVertex3f(-1.0f, 1.0f, -1.0f); // Top Left Of The Quad (Left)
        GL11.glTexCoord2f(0, 0);
        GL11.glVertex3f(-1.0f, -1.0f, -1.0f); // Bottom Left Of The Quad (Left)
        GL11.glTexCoord2f(1, 0);
        GL11.glVertex3f(-1.0f, -1.0f, 1.0f); // Bottom Right Of The Quad (Left)

        GL11.glNormal3f(1, 0, 0);
        GL11.glTexCoord2f(1, 1);
        GL11.glVertex3f(1.0f, 1.0f, -1.0f); // Top Right Of The Quad (Right)
        GL11.glTexCoord2f(0, 1);
        GL11.glVertex3f(1.0f, 1.0f, 1.0f); // Top Left Of The Quad (Right)
        GL11.glTexCoord2f(0, 0);
        GL11.glVertex3f(1.0f, -1.0f, 1.0f); // Bottom Left Of The Quad (Right)
        GL11.glTexCoord2f(1, 0);
        GL11.glVertex3f(1.0f, -1.0f, -1.0f); // Bottom Right Of The Quad (Right)
        GL11.glEnd(); // Done Drawing The Quad
        
//        GL11.glRotated(90, 1, 0, 0);
        GL11.glTranslatef(2.75f, 0, 0);
        Teapot.glutSolidTeapot(1);

        shader.end();
    }

    /**
     * Poll Input
     */
    public void pollInput() {
        Camera.acceptInput(getDelta());
        // scroll through key events
        while (Keyboard.next()) {
            if (Keyboard.getEventKeyState()) {
                if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE)
                    closeRequested = true;
                else if (Keyboard.getEventKey() == Keyboard.KEY_P)
                    snapshot();
                else if(Keyboard.getEventKey() == Keyboard.KEY_1){
                	initShaders("");
                	useGoodSphere = false;
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_2){
                	initShaders("phong");
                	useGoodSphere = true;
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_3){
                	initShaders("wire");
                	useGoodSphere = false;
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_4){
                	initShaders("checker");
                	useGoodSphere = false;
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_5){
                	initShaders("wavy");
                	useGoodSphere = false;
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_6){
                	initShaders("phaser");
                	useGoodSphere = false;
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_7){
                	initShaders("blinn_phong");
                	useGoodSphere = true;
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_Q){
                	lights = Math.min(3,  lights+1);
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_W){
                	lights = Math.max(0, lights-1);
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_A){
                	mats.next();
                }
                
            }
        }

        if (Display.isCloseRequested()) {
            closeRequested = true;
        }
    }

    public void snapshot() {
        System.out.println("Taking a snapshot ... snapshot.png");

        GL11.glReadBuffer(GL11.GL_FRONT);

        int width = Display.getDisplayMode().getWidth();
        int height= Display.getDisplayMode().getHeight();
        int bpp = 4; // Assuming a 32-bit display with a byte each for red, green, blue, and alpha.
        ByteBuffer buffer = BufferUtils.createByteBuffer(width * height * bpp);
        GL11.glReadPixels(0, 0, width, height, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer );

        File file = new File("snapshot.png"); // The file to save to.
        String format = "PNG"; // Example: "PNG" or "JPG"
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
   
        for(int x = 0; x < width; x++) {
            for(int y = 0; y < height; y++) {
                int i = (x + (width * y)) * bpp;
                int r = buffer.get(i) & 0xFF;
                int g = buffer.get(i + 1) & 0xFF;
                int b = buffer.get(i + 2) & 0xFF;
                image.setRGB(x, height - (y + 1), (0xFF << 24) | (r << 16) | (g << 8) | b);
            }
        }
           
        try {
            ImageIO.write(image, format, file);
        } catch (IOException e) { e.printStackTrace(); }
    }
    
    /** 
     * Calculate how many milliseconds have passed 
     * since last frame.
     * 
     * @return milliseconds passed since last frame 
     */
    public int getDelta() {
        long time = (Sys.getTime() * 1000) / Sys.getTimerResolution();
        int delta = (int) (time - lastFrameTime);
        lastFrameTime = time;
     
        return delta;
    }

    private void createWindow() {
        try {
            Display.setDisplayMode(new DisplayMode(960, 540));
            Display.setVSyncEnabled(true);
            Display.setTitle(windowTitle);
            Display.create();
        } catch (LWJGLException e) {
            Sys.alert("Error", "Initialization failed!\n\n" + e.getMessage());
            System.exit(0);
        }
    }
    
    /**
     * Destroy and clean up resources
     */
    private void cleanup() {
        Display.destroy();
    }
    
    public static void main(String[] args) {
        new ShaderApplication().run();
    }
    
    public static class Camera {
        public static float moveSpeed = 0.5f;

        private static float maxLook = 85;

        private static float mouseSensitivity = 0.05f;

        private static Vector3f pos;
        private static Vector3f rotation;

        public static void create() {
            pos = new Vector3f(0, 0, 0);
            rotation = new Vector3f(0, 0, 0);
        }

        public static void apply() {
            if (rotation.y / 360 > 1) {
                rotation.y -= 360;
            } else if (rotation.y / 360 < -1) {
                rotation.y += 360;
            }

            //System.out.println(rotation);
            GL11.glRotatef(rotation.x, 1, 0, 0);
            GL11.glRotatef(rotation.y, 0, 1, 0);
            GL11.glRotatef(rotation.z, 0, 0, 1);
            GL11.glTranslatef(-pos.x, -pos.y, -pos.z);
        }

        public static void acceptInput(float delta) {
            //System.out.println("delta="+delta);
            acceptInputRotate(delta);
            acceptInputMove(delta);
        }

        public static void acceptInputRotate(float delta) {
            if (Mouse.isInsideWindow() && Mouse.isButtonDown(0)) {
                float mouseDX = Mouse.getDX();
                float mouseDY = -Mouse.getDY();
                //System.out.println("DX/Y: " + mouseDX + "  " + mouseDY);
                rotation.y += mouseDX * mouseSensitivity * delta;
                rotation.x += mouseDY * mouseSensitivity * delta;
                rotation.x = Math.max(-maxLook, Math.min(maxLook, rotation.x));
            }
        }

        public static void acceptInputMove(float delta) {
//            boolean keyUp = Keyboard.isKeyDown(Keyboard.KEY_W);
//            boolean keyDown = Keyboard.isKeyDown(Keyboard.KEY_S);
//            boolean keyRight = Keyboard.isKeyDown(Keyboard.KEY_D);
//            boolean keyLeft = Keyboard.isKeyDown(Keyboard.KEY_A);
//            boolean keyFast = Keyboard.isKeyDown(Keyboard.KEY_Q);
//            boolean keySlow = Keyboard.isKeyDown(Keyboard.KEY_E);
//            boolean keyFlyUp = Keyboard.isKeyDown(Keyboard.KEY_SPACE);
//            boolean keyFlyDown = Keyboard.isKeyDown(Keyboard.KEY_LSHIFT);
//
//            float speed;
//
//            if (keyFast) {
//                speed = moveSpeed * 5;
//            } else if (keySlow) {
//                speed = moveSpeed / 2;
//            } else {
//                speed = moveSpeed;
//            }
//
//            speed *= delta;
//
//            if (keyFlyUp) {
//                pos.y += speed;
//            }
//            if (keyFlyDown) {
//                pos.y -= speed;
//            }
//
//            if (keyDown) {
//                pos.x -= Math.sin(Math.toRadians(rotation.y)) * speed;
//                pos.z += Math.cos(Math.toRadians(rotation.y)) * speed;
//            }
//            if (keyUp) {
//                pos.x += Math.sin(Math.toRadians(rotation.y)) * speed;
//                pos.z -= Math.cos(Math.toRadians(rotation.y)) * speed;
//            }
//            if (keyLeft) {
//                pos.x += Math.sin(Math.toRadians(rotation.y - 90)) * speed;
//                pos.z -= Math.cos(Math.toRadians(rotation.y - 90)) * speed;
//            }?"\]=
//            if (keyRight) {
//                pos.x += Math.sin(Math.toRadians(rotation.y + 90)) * speed;
//                pos.z -= Math.cos(Math.toRadians(rotation.y + 90)) * speed;
//            }
        }

        public static void setSpeed(float speed) {
            moveSpeed = speed;
        }

        public static void setPos(Vector3f pos) {
            Camera.pos = pos;
        }

        public static Vector3f getPos() {
            return pos;
        }

        public static void setX(float x) {
            pos.x = x;
        }

        public static float getX() {
            return pos.x;
        }

        public static void addToX(float x) {
            pos.x += x;
        }

        public static void setY(float y) {
            pos.y = y;
        }

        public static float getY() {
            return pos.y;
        }

        public static void addToY(float y) {
            pos.y += y;
        }

        public static void setZ(float z) {
            pos.z = z;
        }

        public static float getZ() {
            return pos.z;
        }

        public static void addToZ(float z) {
            pos.z += z;
        }

        public static void setRotation(Vector3f rotation) {
            Camera.rotation = rotation;
        }

        public static Vector3f getRotation() {
            return rotation;
        }

        public static void setRotationX(float x) {
            rotation.x = x;
        }

        public static float getRotationX() {
            return rotation.x;
        }

        public static void addToRotationX(float x) {
            rotation.x += x;
        }

        public static void setRotationY(float y) {
            rotation.y = y;
        }

        public static float getRotationY() {
            return rotation.y;
        }

        public static void addToRotationY(float y) {
            rotation.y += y;
        }

        public static void setRotationZ(float z) {
            rotation.z = z;
        }

        public static float getRotationZ() {
            return rotation.z;
        }

        public static void addToRotationZ(float z) {
            rotation.z += z;
        }

        public static void setMaxLook(float maxLook) {
            Camera.maxLook = maxLook;
        }

        public static float getMaxLook() {
            return maxLook;
        }

        public static void setMouseSensitivity(float mouseSensitivity) {
            Camera.mouseSensitivity = mouseSensitivity;
        }

        public static float getMouseSensitivity() {
            return mouseSensitivity;
        }
    }
}
