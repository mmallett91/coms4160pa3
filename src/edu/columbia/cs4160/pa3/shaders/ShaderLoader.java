package edu.columbia.cs4160.pa3.shaders;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ShaderLoader {
	
	
	public ShaderLoader(){
		
	}
	
	public String load(String shaderPath){
		
		File file = new File(shaderPath);
		try {
			Scanner in = new Scanner(file);
			StringBuilder sb = new StringBuilder();
			while(in.hasNextLine()){
				sb.append(in.nextLine());
			}
			return sb.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}

}
